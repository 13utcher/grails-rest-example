package rest.example

import grails.converters.JSON

class ArticleController {

    def index() {
        if (params.id) {
            render Article.read(params.id) as JSON
        } else {
            render Article.list() as JSON
        }
    }
}
