package rest.example

import grails.rest.Resource

class Article {

    String barcode
    Long price
    Date dateCreated
    Date lastUpdated
    Coordinate coordinate

    static hasOne = [
            user: User]

    static constraints = {
        barcode nullable: false
        price nullable: false
        coordinate nullable: true
    }
}
