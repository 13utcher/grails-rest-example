package rest.example

class User {

    String name

    static hasMany = [
            articles: Article
    ]

    static constraints = {
        name nullable: false
    }
}
