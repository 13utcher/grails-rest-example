import rest.example.Article
import rest.example.Coordinate
import rest.example.User

class BootStrap {

    def init = { servletContext ->
        def user1 = new User(name: 'test user1').save()
        def user2 = new User(name: 'test user2').save()
        def user3 = new User(name: 'test user3').save()

        def coordinate1 = new Coordinate(latitude: 12.124123, longitude: 43.124123).save()
        def coordinate2 = new Coordinate(latitude: 12.124123, longitude: 43.124123).save()
        def coordinate3 = new Coordinate(latitude: 12.124123, longitude: 43.124123).save()
        def coordinate4 = new Coordinate(latitude: 12.124123, longitude: 43.124123).save()

        def article1 = new Article(barcode: '124125123512', price: 100000, user: user1, coordinate: coordinate1).save()
        def article2 = new Article(barcode: '142342123512', price: 105000, user: user2, coordinate: coordinate2).save()
        def article3 = new Article(barcode: '124125125234', price: 100600, user: user3, coordinate: coordinate3).save()
        def article4 = new Article(barcode: '122342343512', price: 100100, user: user1, coordinate: coordinate3).save()
        def article5 = new Article(barcode: '124125123123', price: 200000, user: user2, coordinate: coordinate4).save()
    }
    def destroy = {
    }
}
